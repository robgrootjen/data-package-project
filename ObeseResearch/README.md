## Obesity death count.
I've been really interested in knowing how much people are actually dying from obesity which causes CHD(Chronic heart diseases). This research will tell you more about it.

## Data sources
Here are the data sources I used.

* Top 10 obese countries - https://www.valuewalk.com/2019/01/top-10-most-obese-countries-oecd-who/
* U.S.A dying rate obesity - https://www.wvdhhr.org/bph/oehp/obesity/mortality.htm#targetText=According%20to%20the%20National%20Institutes,the%20obesity%20epidemic%20(57).
* Risk of dying being obese - https://www.hsph.harvard.edu/news/press-releases/overweight-obesity-mortality-risk/

## Structure analysis
There was not much information about the other countries, and no past data at all. So I did the following. 
1. Took the total population.
2. Took the percentage of population that is obese.
3. Checked out what the risk is of dying being obese. (24%)
4. Divided the obese headcount by the 24%. 

## Files 
* ***datapackage.json*** - All the metadata I used
* ***chart.png*** - Screenshot of bar graph
* ***datavisual.html*** - html code to see the tabular data
* ***csvtojson.py*** - code I used to convert csv to JSON.
* ***obesitytable.png*** - Screenshot of table from HTML/JS

## Obesity death count in 2019 per country quick overview below
<a href="https://ibb.co/253N5N9"><img src="https://i.ibb.co/pnjyny7/visualchart.png" alt="visualchart" border="0"></a>
	
	
	
	
